## IOT Basics

### Industrial Revolution
![Industrial Revolution](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/Annotation_2020-09-11_100545.png)

### Industry 3.0
- In Industry 3.0 Data is stored in databases and
represented in excels.
![Fowchart](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/02.png)  
**Typical Industry 3.0 Architecture :-**
![Architecture](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/03.png)
- Communication Protocols used here are
![communication](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/04.png)  
### Industry 4.0
- Industry 4.0 is Industry 3.0
devices connected to the
Internet  
![](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/05.jpg)  
- when we connect devices to Internet,they send us the data.
- What can we do that data gathered from devices?
![](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/06.png)
- We have seen the typical Industry 3.0 Architecture. For that we will be adding edge block block and cloud block.
- Edge Block work : Data data goes from Controller to Cloud via the Industry 4.0 protocols.
- Typical Industry 4.0 Architecture:
![](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/07.png)
- Industry 3.0 to Industry 4.0
![](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/08.png)
- Considerations taken when an Industry 3.0 has to be converted into Industry 4.0 :
  - **Cost**
  - **Downtime**
  - **Reliability**
- Solution is to make low cost devices which Get data from Industry 3.0 devices/meters/sensors without changes to the original device.
- Library that helps get data from Industry 3.0 devices and send to Industry 4.0 cloud.
![](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/09.png)
- **Roadmap for making Industrial IoT product**
 - Identify most popular Industry 3.0 devices.
 - Step 2 : Study Protocols that these devices Communicate
 - Step 3 : Get data from
the Industry 3.0 devices
 - Step 4 : Send the data to
cloud for Industry 4.0
- The data gets stored in TSDB(Time Series DataBase) like Prometheus,InfluxDB.
- This helps to make IoT Dash boards like Grafana , Thingsboard  
 Graphana ![Graphana Dashboard](http://grafana.org/assets/img/features/dashboard_ex1.png)
 Thingsboard ![Thingsboard](https://4.bp.blogspot.com/-s0efNwrsS1E/WF0sSnCvZ3I/AAAAAAAAAA0/2Ko4JnBjW7Q_yBpLRtF80IGa9fpfkQvkwCLcB/s1600/facilities-management.png)
 ### IOT Platforms
![](https://www.google.com/url?sa=i&url=https%3A%2F%2Fmedium.com%2Fschaffen-softwares%2Fpart-4-iot-platforms-b8f2c4e4639b&psig=AOvVaw0rzRPS_dvfInf3J6dGHPEv&ust=1600325081104000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNiUhdKJ7esCFQAAAAAdAAAAABAq)
- Analyse your data on these platforms. The following help to perform analytics.
  - AWS IOT
  - Google IoT
  - Azure IoT
  - Thingsboard(It also a dashboard)
- Using Zaiper/Twilio we can get alerts based om your data using these platforms.
![](https://gitlab.com/SaiTejaMutchi/iotmodule1/-/raw/master/assignments/summary/extras/10.png)
- For a device we need two important things.
  - **Interface**
  - **Protocol**
- For device we need to connect it via interface and then we need to know the address of what has to be read.Read it through and add to the cloud.
- Some companies provide address in the manual.Then it would be easy  to convert Industry 3.0 to Industry 4.0. If not we have to contact the manufacturer.






